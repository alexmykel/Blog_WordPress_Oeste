<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'western');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '>(Xd#|o7v,3mjOx3kZ8l#Y2`V>5HAkl^*o|jb:^.{^T&OW+_SdD,(dHu(tfD%?N3');
define('SECURE_AUTH_KEY', 'kfU:{&O@oXe@VPdTm(ssn?&7]eiOoT6qDv>3Hn?d-2}s$[Q/n@)!P)#!gk/@8&7V');
define('LOGGED_IN_KEY', 'XsSXB[zdr|:8IP~G`kl,f.j1n^%5Uf{m,H4%AP3L?(P:HY1Z%SH%Q1r7 yx .XCg');
define('NONCE_KEY', 'kdEo@LAqf;4Ypx<6D)j]<u+p1e>A]o;0dqRc0Y9ahv|5 -@7Jv1Z1DX!g+p0N?:d');
define('AUTH_SALT', 'b ?WyRBh&2NR!4wPOkR+xpksQ;u.?Q}:k%</ysB]L$MM^(,77X_Z{xVaZn8V)gnY');
define('SECURE_AUTH_SALT', 'Xz>[+2g0g7o]xBm}5t#UMT{y.m(k?xWM]we]}GblM7/hO+Mtx,--x zTX9lDCLgU');
define('LOGGED_IN_SALT', 'GZ#Ww84+)dZ?W,q[~)cmv:OCZ2-jOTZc^a0UMO`zw>FO|/efHSg<#e3W(Xtu@S*%');
define('NONCE_SALT', '7~plyzw3s=g^ `M&+{&31@xL`32Mn{h1Sl!P}.]{z0a@hNE,ZT.!4wdd~F|)qUpe');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_western';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

